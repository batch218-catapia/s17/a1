/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	let fullNamePrompt = prompt("Enter you full name:")
    console.log("Hello, " +fullNamePrompt);

    	let agePrompt = prompt("Enter you age:")
    console.log("You are " + agePrompt + " years old.");

    	let locPrompt = prompt("Enter your Location:")
    console.log("You live in " + locPrompt + " City");

/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function displayTopBand(){
    console.log("1. Kamikazee");
    console.log("2. Queens");
    console.log("3. Imagine Dragon");
    console.log("4. Moment of Truth");
    console.log("5. Creed");

}

displayTopBand();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function displayMovieRating(){
    console.log("1. Avengers : EndGame");
    console.log("Rotten Tomatoes Rating : 94%");
    console.log("2. Avengers : Infinity War");
    console.log("Rotten Tomatoes Rating : 95%");
    console.log("3. Iron Man 3");
    console.log("Rotten Tomatoes Rating : 79%");
    console.log("4. Man Of Steel");
    console.log("Rotten Tomatoes Rating : 56%");
    console.log("5. Godzilla");
    console.log("Rotten Tomatoes Rating : 76%");
    
}

displayMovieRating();

/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	// let fullNamePrompt = prompt("Enter you full name:")
 //    console.log("Hello, " +fullNamePrompt);

	function  printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");


	console.log("You are friends with: " + friend1);
	console.log("You are friends with: " + friend2);
	console.log("You are friends with: " + friend3);
	// console.log(friends) ;

}
	printUsers();

/*console.log(friend1);
console.log(friend2);*/